# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
use Mix.Config

# Define application's host and port (defaults to "http://localhost:4001")
#config :hound, app_host: "http://localhost", app_port: 4000

# Start with selenium driver (default)
config :hound, driver: "selenium"

# Use Chrome with the default driver (selenium)
#config :hound, browser: "chrome"

# Start with default driver at port 1234 and use firefox
#config :hound, port: 1234, browser: "firefox"

# Start Hound for PhantomJs
#config :hound, driver: "phantomjs"

# Start Hound for ChromeDriver (default port 9515 assumed)
#config :hound, driver: "chrome_driver"


config :poc_elixir_docker_app,
  ecto_repos: [PocElixirDockerApp.Repo]

# Configures the endpoint
config :poc_elixir_docker_app, PocElixirDockerAppWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "Aq4qU9deLvYYn2N9RZizsmPyvdz6wZqeH8+BPa1C6Um23FkGmjiy2om/RJZ1PCMG",
  render_errors: [view: PocElixirDockerAppWeb.ErrorView, accepts: ~w(html json)],
  pubsub: [name: PocElixirDockerApp.PubSub, adapter: Phoenix.PubSub.PG2]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"

config :junit_formatter,
  report_file: "report_file_test.xml",
  print_report_file: true
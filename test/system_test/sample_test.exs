defmodule.PocElixirDockerApp.SampleTest do 
    use PocElixirDockerApp.ConnCase

    # import hound helpers 
    use Hound.Helpers 

    # Start hound session
    hound_session
        IO.puts "hai"
    test "GET /" do 
        navigate_to ("http://localhost:4000")
        :timer.sleep(5000) # puts server to sleep
        assert page_source =~ "Welcome to phoenix"
    end
end
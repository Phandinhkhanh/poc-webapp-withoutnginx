# ExUnit.configure formatters: [JUnitFormatter]
# ExUnit.start()
# Ecto.Adapters.SQL.Sandbox.mode(PocElixirDockerApp.Repo, :manual)
{:ok, _} = Application.ensure_all_started(:hound)
ExUnit.configure formatters: [JUnitFormatter]
ExUnit.start